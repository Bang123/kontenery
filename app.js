var express     = require("express"),
    app         = express();
    require('dotenv').config();

var indexRoutes = require("./routes/index"),
    konteneryRoutes = require("./routes/kontenery");

app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));

app.use("/", indexRoutes);
app.use("/kontenery", konteneryRoutes);


app.listen(process.env.PORT, process.env.IP, function(){
   console.log("Kontenery App Has Started!");
});