var express = require("express");
var router  = express.Router();


//root route
router.get("/", function(req, res){
    res.render("landing");
});

router.get("/kontakt", function(req, res){
    res.render("kontakt");
});

module.exports = router;